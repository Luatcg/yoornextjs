import type { NextPage } from "next";
import Head from "next/head";
import Article from "../layout/module/article";
import Paginator from "../layout/pagination";
import SearchBox from "../layout/search-box";
import { useGetPosts } from "./api/posts";

const Home: NextPage = () => {
  const res = useGetPosts();
  console.log("data", res.data);
  return (
    <div className="l-content">
      <Head>
        <title>yoor公式ブログ</title>
        <meta name="description" content="Yoor Blog Seesaa" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="Main">
        <div className="Main__contents">
          {res?.data?.items.map((post) => (
            <Article key={post.id} data={post} />
          ))}
        </div>
        <Paginator></Paginator>
      </div>
      <SearchBox></SearchBox>
    </div>
  );
};

export default Home;
