import type { NextPage } from "next";
import { useRouter } from "next/router";
import Moment from "react-moment";
import SearchBox from "../../layout/search-box";
import { useGetPostDetail } from "../api/posts/queries";
import ReactHtmlParser from "react-html-parser";
import Head from "next/head";

const ArticleDetail: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const post = useGetPostDetail({ id });
  console.log(post);
  return (
    <div className="l-content">
      <Head>
        <title>yoor || {post?.data?.title}</title>
      </Head>

      <div className="Main">
        <div className="Main__contents">
          <article className="Article">
            <header className="Article__heading">
              <p className="date">
                <span>
                  <Moment format="YYYY年M月D日">{post?.data?.createdAt}</Moment>
                </span>
              </p>
              <h2 className="title">
                <span>{post?.data?.title}</span>
              </h2>
            </header>
            <div className="Article__content">
              {ReactHtmlParser(post?.data?.content || "")}
            </div>
          </article>
        </div>
      </div>
      <SearchBox />
    </div>
  );
};

export default ArticleDetail;
