import { PagedMeta } from "../../../styles/util.types";

export interface IPostParams {
    page?: number;
    limit?: number;
}

export interface IPosts {
    items: IPost[];
    meta: PagedMeta;
}

export interface IPost {
    id?: string;
    image?: string;
    title?: string;
    content?: string;
    closedAt?: string;
    createdAt?: string;
    updatedAt?: string;
    publishedAt?: string;
}

export interface IPostDetailParams {
    id?: any;
}

export const LIMIT_POST = 10;
