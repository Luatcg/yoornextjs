export {getPosts, getPostDetail} from './request';
export type {IPost, IPostParams, IPosts} from './types';
export {useGetPosts} from './queries';
