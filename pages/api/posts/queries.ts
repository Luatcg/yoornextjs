import {
  useQuery,
  UseInfiniteQueryOptions,
  UseQueryOptions,
} from "react-query";
import {
  IPostParams,
  IPosts,
  IPostDetailParams,
  LIMIT_POST,
  IPost,
} from "./types";
import { getPostDetail, getPosts } from "./request";

export const useGetPosts = (
  filterParams?: IPostParams,
  option?: UseQueryOptions<IPosts, Error>
) => {
  return useQuery<IPosts, Error>(
    ["/post", filterParams],
    ({ pageParam = 1 }) => {
      const params: IPostParams = {
        ...filterParams,
        limit: LIMIT_POST,
        page: pageParam,
      };
      return getPosts(params);
    },
    option
  );
};

export const useGetPostDetail = (
  params: IPostDetailParams,
  option?: UseQueryOptions<IPost, Error>
) => {
  return useQuery<IPost, Error>(
      `/post/${params.id}`,
      () => getPostDetail(params),
      option
  );
};
