import {IPostParams, IPosts,IPost, IPostDetailParams} from "./types";
import {request} from '../axios';

export const getPosts = async (params: IPostParams): Promise<IPosts> => {
    const res: any = await request({
        url: '/post',
        method: 'GET',
        params: params
    });

    console.log(res);
    return {
        items: res.data,
        meta: res.meta
    }
};


export const getPostDetail = async (params: IPostDetailParams): Promise<any> => {
    const res: any = await request({
        url: `/post/${params.id}`,
        method: "GET",
        params: params,
    });

    return res
};
