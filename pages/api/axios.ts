import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

// @ts-ignore
import { API_URL } from '@/lib/constants';
import { AxiosError } from 'axios';

export const request = axios.create({
    baseURL: 'https://jindoku-yoor-blog-dev.hacocms.com/api/v1'
});

const handleError = async (error: AxiosError) => {
    const data = error?.response?.data;

    // @ts-ignore
    return Promise.reject(data?.meta || data || error);
};

const handleSuccess = async (res: AxiosResponse) => {
    return res.data;
};

const handleRequest = async (config: AxiosRequestConfig) => {
    const token = 'NQotTfmvgtbXe9y7MqBK6sVJ';
    const draftToken = '5RjeGqcgYeQYUnjcpc7hdgfA';
    if (token) {
        config = {
            ...config,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };
    }

    return config;
};

request.interceptors.response.use(handleSuccess, handleError);

request.interceptors.request.use(handleRequest, (error) => Promise.reject(error));
