import '../styles/globals.css'
import type { AppProps } from 'next/app'
import '../styles/common.css'
import  '../styles/user-common.css'
import Layout from "../layout";
import { QueryClient, QueryClientProvider } from "react-query";
import React from "react";
import { ReactQueryDevtools } from "react-query/devtools";


function MyApp({Component, pageProps}: AppProps) {
  const [queryClient] = React.useState(() => new QueryClient());
  return (
      <QueryClientProvider client={queryClient}>
          <Layout>
              <Component {...pageProps} />
          </Layout>
          <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
  )
}

export default MyApp
