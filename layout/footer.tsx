const Footer = () => {
  return (
    <div>
      <footer role="contentinfo" className="l-footer">
        <div className="Footer"></div>
        <div className="Poweredby">
          <div className="Poweredby__body">
            <a href="https://blog.seesaa.jp">
              <img src="/seesaa.svg" alt=""></img>
            </a>
            <a href="https://blog.seesaa.jp">
              <img src="/blog.svg" alt=""></img>
            </a>
          </div>
        </div>
      </footer>
    </div>
  );
};
export default Footer;
