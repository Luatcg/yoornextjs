const Header = () => {
  return (
    <div>
      <header role="banner" className="l-header">
        <div className="Header">
          <div className="Header__inner">
            <h1>
              <a href="https://blog.yoor.jp/">
                <span>yoor公式ブログ</span>
              </a>
            </h1>
          </div>
        </div>
      </header>
    </div>
  );
};
export default Header;
