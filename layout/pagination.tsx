const Pagination = () => {
  return (
    <ul className="Pager">
      <li className="Pager__item -prev -disabled">
        <span>&lt;</span>
      </li>
      <li className="Pager__item -active">
        <span>1</span>
      </li>
      <li className="Pager__item">
        <a href="https://blog.yoor.jp/index-2.html">2</a>
      </li>
      <li className="Pager__item">
        <a href="https://blog.yoor.jp/index-3.html">3</a>
      </li>
      <li className="Pager__item">
        <a href="https://blog.yoor.jp/index-4.html">4</a>
      </li>
      <li className="Pager__item">
        <a href="https://blog.yoor.jp/index-5.html">5</a>
      </li>
      <li className="Pager__item">
        <span>..</span>
      </li>
      <li className="Pager__item -next">
        <a href="https://blog.yoor.jp/index-2.html">&gt;</a>
      </li>
    </ul>
  );
};
export default Pagination;
