const SearchBox = () => {
  return (
    <aside role="complementary" className="Side">
      <section className="Module -search">
        <h3 className="Module__heading -none">検索</h3>
        <div className="Module__body">
          <form
            method="get"
            action="https://blog.yoor.jp/search"
            className="Search"
          >
            <input
              name="keyword"
              type="text"
              placeholder="ブログ内検索"
              className="Search__text"
            ></input>
            <input
              value="検索"
              type="submit"
              className="Search__submit"
            ></input>
          </form>
        </div>
        <div className="dparts -one"></div>
        <div className="dparts -two"></div>
        <div className="dparts -three"></div>
        <div className="dparts -four"></div>
        <div className="dparts -five"></div>
      </section>
    </aside>
  );
};
export default SearchBox;
