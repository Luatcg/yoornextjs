export type PagedMeta = {
    total: number;
    limit: number;
    offset: number;
  };

